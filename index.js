let express = require('express');
let app = express();
const PORT = 3000;
let mongoose = require('mongoose');
let usersRoutes = require('./routes/usersRoutes');


app.listen(PORT, ()=> console.log('Server is running'));
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use("/", usersRoutes);

mongoose.connect("mongodb+srv://kielpaulo:damienzk@cluster0.y6fy9.mongodb.net/s31?retryWrites=true&w=majority",{

	useNewUrlParser:true,
	useUnifiedTopology: true
})
.then(()=>console.log('Connected to the database'))
.catch((error)=>console.log(error));



