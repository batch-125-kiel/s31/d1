let User = require('./../models/User');




//insert new user
module.exports.createUser = function (reqBody){

	let newUser = new User({

		firstName:reqBody.firstName,
		lastName: reqBody.lastName,
		userName: reqBody.userName,
		password: reqBody.password

	});
	
	return newUser.save().then((savedUser, error)=>{

		if(error){
			console.log('MAY ERROR');
			return error;

		}else{

			console.log('WALA ERROR, NAG ADD SA DATABASE');
			return savedUser;
			
		}

	});

}

module.exports.retrieveUsers = () =>{

	return User.find().then((retrievedUsers, error)=>{

		if(error){
			console.log('MAY ERROR');
			return error;

		}else{
			console.log('WALA ERROR');
			return retrievedUsers;
		}

	});

}

module.exports.retrieveUser = (id) =>{

	return User.findById(id).then((retrievedUser, error)=>{

		if(error){
			console.log('MAY ERROR');
			return error;

		}else{
			console.log('WALA ERROR');
			return retrievedUser;
		}

	});
}

module.exports.editUser = (id, body)=>{

	return User.findByIdAndUpdate(id,body,{new: true}).then((updatedUser, error)=>{

		if(error){

			console.log('MAY ERROR');
			return error;

		}else{

			console.log('WALA ERROR');
			return updatedUser;

		}

	});

}
module.exports.deleteUser = (id)=>{

	return User.findByIdAndDelete(id).then((deletedUser, error)=>{

		if(error){

			console.log('MAY ERROR');
			return error;

		}else{

			console.log('WALA ERROR, SUCCESFULLY DELETED');
			return deletedUser;
		}

});


}


