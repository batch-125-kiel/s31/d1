let express = require('express');
let router =  express.Router();
let User = require('./../models/User');
let userController = require('./../controllers/userControllers');



//ROUTES
router.post('/add-user',(req, res)=>{

	// chuii then
	userController.createUser(req.body).then((savedUser) => res.send(savedUser));
	
});
//RETRIEVE ALL
router.get('/users', (req, res)=>{

	userController.retrieveUsers().then((retrievedUsers) => res.send(retrievedUsers));

});

//RETRIEVE SPECIFIC USER by ID
router.get("/users/:id", (req, res)=>{

	let id = req.params.id;
	userController.retrieveUser(id).then((retrievedUser) => res.send(retrievedUser));

})

//UPDATE USERS
router.put("/user/:id",(req, res)=>{

	let id = req.params.id;
	let body = req.body
	userController.editUser(id, body).then((updatedUser) => res.send(updatedUser));


});

//DELETE
router.delete("/user/:id", (req, res)=>{

	let id = req.params.id
	userController.deleteUser(id).then((deletedUser) => res.send(deletedUser));

});



module.exports = router;


