let mongoose = require('mongoose');

//SCHEMA
const userSchema = new mongoose.Schema({

	firstName: String,
	lastName: String,
	userName: String,
	password: String

});

//MODEL
module.exports = mongoose.model('User', userSchema);